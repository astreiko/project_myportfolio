import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import {HomeAppComponent} from "./pages/home-app/home-app.component";
import {Project1Module} from "./pages/page-progect-1/project1.module";
import {Project1Component} from "./pages/page-progect-1/project1.component";
import {MainAppComponent} from "./shared/components/main-app/main-app.component";
import {Project3Module} from "./pages/page-progect-3/components/project3.module";
import {Project2Module} from "./pages/page-progect-2/components/project2.module";
import {HeaderAppComponent} from "./shared/components/header-app/header-app.component";
import {Project3Component} from "./pages/page-progect-3/components/project3.component";
import {Project2Component} from "./pages/page-progect-2/components/project2.component";

@NgModule({
  declarations: [
    AppComponent,
    MainAppComponent,
    HomeAppComponent,
    Project3Component,
    Project2Component,
    Project1Component,
    HeaderAppComponent,
  ],
  imports: [
    BrowserModule,
    Project3Module,
    Project2Module,
    Project1Module,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
