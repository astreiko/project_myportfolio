import { NgModule } from '@angular/core';

import { Project1Component } from "./project1.component";
import { BrowserModule } from '@angular/platform-browser';
import { PageOneComponent } from "./components/page-one/page-one.component";

@NgModule({
  declarations: [
    PageOneComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  exports: [
    PageOneComponent,
  ],
  bootstrap: [Project1Component]
})
export class Project1Module {}
