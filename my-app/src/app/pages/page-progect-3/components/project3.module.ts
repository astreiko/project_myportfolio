import { NgModule } from '@angular/core';

import { Project3Component } from "./project3.component";
import { BrowserModule } from '@angular/platform-browser';
import { HomeAppComponent } from "./home-app/home-app.component";
import { MenuAppComponent } from "./menu-app/menu-app.component";
import { LoginAppComponent } from "./login-app/login-app.component";
import { BasicAppComponent } from "./basic-app/basic-app.component";
import { FooterAppComponent } from "./footer-app/footer-app.component";
import { SearchAppComponent } from "./search-app/search-app.component";
import { TravelAppComponent } from "./travel-app/travel-app.component";
import { RewiewsAppComponent } from "./rewiews-app/rewiews-app.component";
import { GalleryAppComponent } from "./gallery-app/gallery-app.component";
import { ContactsAppComponent } from "./contacts-app/contacts-app.component";
import { HolidaysAppComponent } from "./holidays-app/holidays-app.components";
import { SocialNavComponent } from "./shared/social-nav/social-nav.component";

@NgModule({
  declarations: [
    MenuAppComponent,
    HomeAppComponent,
    BasicAppComponent,
    LoginAppComponent,
    FooterAppComponent,
    TravelAppComponent,
    SearchAppComponent,
    SocialNavComponent,
    GalleryAppComponent,
    RewiewsAppComponent,
    HolidaysAppComponent,
    ContactsAppComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  exports: [
    MenuAppComponent,
    HomeAppComponent,
    LoginAppComponent,
    FooterAppComponent,
    TravelAppComponent,
    SearchAppComponent,
    GalleryAppComponent,
    RewiewsAppComponent,
    HolidaysAppComponent,
    ContactsAppComponent,
  ],
  bootstrap: [Project3Component]
})
export class Project3Module { }
