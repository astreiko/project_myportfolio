import { NgModule } from '@angular/core';

import { ManComponent } from "./man/man.component";
import { LogoComponent } from "./logo/logo.component";
import { Line5Component } from "./line5/line5.component";
import { Line6Component } from "./line6/line6.component";
import { Line7Component } from "./line7/line7.component";
import { Line8Component } from "./line8/line8.component";
import { Project2Component } from "./project2.component";
import { Line3Component } from "./line3/line3.component";
import { Line4Component } from "./line4/line4.component";
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    LogoComponent,
    ManComponent,
    Line3Component,
    Line4Component,
    Line5Component,
    Line6Component,
    Line7Component,
    Line8Component,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  exports: [
    LogoComponent,
    ManComponent,
    Line3Component,
    Line4Component,
    Line5Component,
    Line6Component,
    Line7Component,
    Line8Component,
  ],
  bootstrap: [Project2Component]
})
export class Project2Module {}
