import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeAppComponent } from "./pages/home-app/home-app.component";
import { Project1Component } from "./pages/page-progect-1/project1.component";
import { Project3Component } from "./pages/page-progect-3/components/project3.component";
import { Project2Component } from "./pages/page-progect-2/components/project2.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeAppComponent,
  },
  {
    path: 'project3',
    component: Project3Component,
  },
  {
    path: 'project2',
    component: Project2Component,
  },
  {
    path: 'project1',
    component: Project1Component,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
